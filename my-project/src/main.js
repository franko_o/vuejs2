import Vue from 'vue';
import App from './App';
import router from './router';
import { store } from './store/store';

Vue.config.productionTip = false;

/** Global Vue application configuration data **/
new Vue({
  el: '#app',
  store: store,
  router,
  components: { App },
  template: '<App/>'
});
