import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        users: [
            {
                id : 1,
                name: "John Small",
                email: "FISincere@april.biz",
                avatar: "https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Small-world-network-example.png/220px-Small-world-network-example.png"
            },
            {
                id : 2,
                name: "Big Vasya",
                email: "april@Since.com",
                avatar: "http://mxtrianz.me/wp-content/uploads/2017/05/small-photos-17-the-data-lab.jpg"
            }
        ],
        albums: [
            {
                id : 1,
                title: "Summer",
                userId: 1,
                preview: "https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Small-world-network-example.png/220px-Small-world-network-example.png"
            },
            {
                id : 2,
                title: "Autumn",
                userId: 1,
                preview: "http://mxtrianz.me/wp-content/uploads/2017/05/small-photos-17-the-data-lab.jpg"
            },
            {
                id : 3,
                title: "Winter",
                userId: 2,
                preview: "https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Small-world-network-example.png/220px-Small-world-network-example.png"
            },
            {
                id : 4,
                title: "Spring",
                userId: 2,
                preview: "http://mxtrianz.me/wp-content/uploads/2017/05/small-photos-17-the-data-lab.jpg"
            }
        ],
        photos: [
            {
                id: 1,
                albumId : 1,
                url: "http://mxtrianz.me/wp-content/uploads/2017/05/small-photos-17-the-data-lab.jpg",
                title: 'Title 1'
            },
            {
                id: 2,
                albumId : 2,
                url: "http://mxtrianz.me/wp-content/uploads/2017/05/small-photos-17-the-data-lab.jpg",
                title: 'Title 1'
            },
            {
                id: 3,
                albumId : 3,
                url: "http://mxtrianz.me/wp-content/uploads/2017/05/small-photos-17-the-data-lab.jpg",
                title: 'Title 1'
            },
            {
                id: 4,
                albumId : 4,
                url: "http://mxtrianz.me/wp-content/uploads/2017/05/small-photos-17-the-data-lab.jpg",
                title: 'Title 1'
            },
            {
                id: 5,
                albumId : 1,
                url: "https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Small-world-network-example.png/220px-Small-world-network-example.png",
                title: '--Title 2--'
            },
            {
                id: 6,
                albumId : 2,
                url: "https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Small-world-network-example.png/220px-Small-world-network-example.png",
                title: '--Title 2--'
            },
            {
                id: 7,
                albumId : 3,
                url: "https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Small-world-network-example.png/220px-Small-world-network-example.png",
                title: '--Title 2--'
            },
            {
                id: 8,
                albumId : 4,
                url: "https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Small-world-network-example.png/220px-Small-world-network-example.png",
                title: '--Title 2--'
            },
        ]
    },
    getters: {
        filterUsers: (state, getters) => (key) => {
            return state.users.filter(function(user) {
                return user.name.toLowerCase().includes(key) || user.email.toLowerCase().includes(key);
            });
        },
        getUserById: (state, getters) => (id) => {
            return state.users.find(user => user.id === parseInt(id))
        },
        getAlbums: state => {
            return state.albums;
        },
        filterAlbumByUserId: (state, getters) => (id) => {
            if (! id) return state.albums;
            return state.albums.filter(function(album) {
                return album.userId === parseInt(id);
            });
        },
        getAlbumById: (state, getters) => (id) => {
            return state.albums.find(album => album.id === parseInt(id))
        },
        getPhotosByAlbum: (state, getters) => (id) => {
            return state.photos.filter(photo => photo.albumId === parseInt(id));
        }
    },
    mutations: {
        addUser: (state, user) => {
            state.users.push(user);
        },
        updateUser: (state, user) => {
            let users = state.users;
            $.each(users, function(i){
                if(users[i].id === parseInt(user.id)) {
                    // TODO: refactor
                    users[i].name = user.name;
                    users[i].email = user.email;
                    users[i].avatar = user.avatar;
                    return false;
                }
            });
            state.users = users;
        },
        deleteUser: (state, userId) => {
            let users = state.users;
            $.each(users, function(i){
                if(users[i].id === userId) {
                    users.splice(i,1);
                    return false;
                }
            });
            state.users = users;
        },
        addAlbum: state => {
            var newAlbum = {
                id : 5,
                name: "5User2",
                userId: 2,
                preview: "http://mxtrianz.me/wp-content/uploads/2017/05/small-photos-17-the-data-lab.jpg"
            };
            state.albums.push(newAlbum);
        },
        updateAlbum: (state, album) => {
            let albums = state.albums;
            $.each(albums, function(i){
                if(albums[i].id === parseInt(album.id)) {
                    // TODO: refactor
                    albums[i].title = album.title;
                    albums[i].preview = album.preview;
                    return false;
                }
            });
            state.albums = albums;
        },
        deleteAlbum: (state, albumId) => {
            let albums = state.albums;
            $.each(albums, function(i){
                if(albums[i].id === albumId) {
                    albums.splice(i,1);
                    return false;
                }
            });
            state.albums = albums;
        },
        addPhoto: (state, photo) => {
            state.photos.push(photo);
        },
        deletePhoto: (state, photoId) => {
            let photos = state.photos;
            $.each(photos, function(i){
                if(photos[i].id === photoId) {
                    photos.splice(i,1);
                    return false;
                }
            });
            state.photos = photos;
        }
    },
    actions: {
        storeNewUser: (context, newUser) => {
            context.commit('addUser', newUser);
        },
        updateUser: (context, updatedUser) => {
            context.commit('updateUser', updatedUser);
        },
        deleteUser: (context, userId) => {
            if (confirm('Are you sure you want to delete this user?')) {
                context.commit('deleteUser', userId);
            }
        },

        addAlbum: context => {
            context.commit('addAlbum');
        },
        updateAlbum: (context, album) => {
            context.commit('updateAlbum', album);
        },
        deleteAlbum: (context, albumId) => {
            if (confirm('Are you sure you want to delete this album?')) {
                context.commit('deleteAlbum', albumId);
            }
        },

        storeNewPhoto: (context, photo) => {
            context.commit('addPhoto', photo);
        },
        deletePhoto: (context, photoId) => {
            context.commit('deletePhoto', photoId);
        },
    }
});