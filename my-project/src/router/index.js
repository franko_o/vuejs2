import Vue from 'vue';
import Router from 'vue-router';

import UserList from '../components/User/UserList.vue';
import UserItem from '../components/User/UserItem.vue';
import UserAdd from '../components/User/UserAdd.vue';
import UserEdit from '../components/User/UserEdit.vue';

import AlbumList from '../components/Album/AlbumList.vue';
import AlbumItem from '../components/Album/AlbumItem.vue';
import AlbumEdit from '../components/Album/AlbumEdit.vue';

import PhotoAdd from '../components/Photo/PhotoAdd.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
        path: '/users/add',
        component: UserAdd,
        name: 'UserAdd',
    },
    {
      path: '/users/:id/edit',
      component: UserEdit,
      name: 'UserEdit',
    },
    {
        path: '/users',
        component: UserList,
        name: 'UserList',
    },
    {
      path: '/users/:id',
      component: UserItem,
      name: 'UserItem',
    },


    {
      path: '/albums/:id/photo/add',
      component: PhotoAdd,
      name: 'PhotoAdd',
    },
    {
      path: '/albums/:id/edit',
      component: AlbumEdit,
      name: 'AlbumEdit',
    },
    {
        path: '/albums',
        component: AlbumList,
        name: 'AlbumList',
    },
    {
      path: '/albums/:id',
      component: AlbumItem,
      name: 'AlbumItem',
    },
  ]
})
